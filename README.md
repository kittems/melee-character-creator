# Melee Character Creator

This is a basic melee character creator written in react.

The 'entry point' is `App.js` towards the bottom on class App's render method.

## Install

The dependencies are all currently pulling from the internet to show easy proof of concept.

However, I did need a basic webserver so it would pull files correctly. Pull it down with NPM.

```bash
npm install
```

Then you can just start up the basic webserver (LINUX)

```bash
npm run start
```

Because of pathing (grr \ or /), if you're on windows, this one works too

```bash
npm run start-win
```

Navigate to `localhost:8080` for the application.