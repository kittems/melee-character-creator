/**
 * This is a variable that defines available races
 */
const Races = {
  'human': {
    age: {
      young: 30,
      old: 60,
      unrealistic: 120
    }
  },
  'orc': {
    age: {
      young: 10,
      old: 40,
      unrealistic: 100
    }
  },
  'dwarf': {
    age: {
      young: 60,
      old: 150,
      unrealistic: 400
    }
  },
  'elf': {
    age: {
      young: 100,
      old: 300,
      unrealistic: 900
    }
  }
}

/**
 * Shows all the available classes
 */
const Classes = {
  'rogue': {},
  'fighter': {},
  'wizard': {}
}

/**
 * A very simple component that just wraps the field for CSS
 * class adding. Also allows optional label
 * @param {*} props 
 */
function Field(props) {
  return (
    <div className="editor-field">
      { /* this fancy line says to put a label element if they give us one */ }
      { ( props.label ) ? <label>{ props.label }: </label> : '' }
      { /* if you have a child element in react, this will render them. */ }
      { props.children }
    </div>
  )
}

/**
 * This is a component that simply displays our character
 * @param {*} props name, age, sex, race, class, etc.
 */
function CharDisplay(props) {
  return (
    <div>
      <h4>Character Info</h4>
      <h1>{ props.name }</h1>
      <h4>{ props.sex } { props.race } {props.class}</h4>
      <p>
        { props.backstory }
      </p>
      { /* we only want to include these IF we age age or height */
        ( props.age || props.height ) ?
        <div>
          <h4>Statistics</h4>
          <hr />
          <table>
            <thead>
              <tr>
                <th>Age</th>
                <th>Height</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{ props.age }</td>
                <td>{ props.height }</td>
              </tr>
            </tbody>
          </table>
        </div>
        : ''
      }
    </div>
  )
}

/**
 * Selector showcases how we could make a component that doesn't care what
 * specifically it is showing, it will just show any Object's keys as a list of
 * fields. Useful for 'class' and 'race.'
 * @param {*} props 
 */
function ListSelector(props) {
  // this will store the elements
  let options = [];
  // we want to create an option for each value
  // in react, you need to pass in a unique row 'key' when making an array so it knows not to rerender.
  for (let value in props.items) {
    options.push(
      <option value={value} key={value}>{ value }</option>
    );
  }

  return (
    <select value={ props.value } onChange={ props.onChange }>
      { options }
    </select>
  );
}

function SexSelector(props) {
  // this is just showing you can make easy variables here
  const isMale = props.sex === 'male';
  const isFemale = props.sex === 'female';

  return (
    <Field>
      <div>
        <input type="radio" name="sex" value="male" checked={isMale} onChange={props.onChange} />
        <label htmlFor="male">Male</label>
      </div>
      <div>
        <input type="radio" name="sex" value="female" checked={isFemale} onChange={props.onChange} />
        <label htmlFor="female">Female</label>
      </div>
    </Field>
  )
}

/**
 * This component will give us useful information
 * about the age we entered.
 * @param {*} props race, age
 */
function AgeInfo(props) {
  // make sure we have a valid age and race, otherwise be blank
  if (props.age == null || isNaN(props.age) || !(props.race in Races))
    return <div></div>

  // based on our race, is our age appropriate?
  const ageData = Races[props.race].age;
  // for each boundary, make a message. We could just use a variable and one 'return'
  // but I wanted to show how you can return like this in the render method
  if (props.age <= ageData.young) {
    return <div className="text-ok">You are a young { props.race }</div>
  } else if (props.age <= ageData.old ) {
    return <div>You are an adult { props.race }</div>
  } else if ( props.age <= ageData.unrealistic ) {
    return <div className="text-warn">You are an old { props.race }</div>
  } else {
    return <div className="text-danger">You are probably a dead { props.race }</div>
  }
}

function App() {
  const [ char, setChar ] = useState({
    name: '',
    age: '',
    sex: 'male',
    race: 'human',
    class: 'fighter',
    backstory: ''
  });

  /**
   * This is a function that will create a new function that can change state.
   * () => { ... } is a shortcut syntax for function() { ... }.bind(this);
   */
  const handleChange = (field) => {
    /* the below line creates a new function that will set this component's state
     * to be whatever is passed in by the event. We could've just made 6-7 functions,
     * each for 'age' and 'sex', but this is convenient.
     */
    return (event) => {
      setChar({
        [field]: event.target.value
      })
    }
  }

  /**
   * This is a handler for a change on the Age field.
   * This one is different than the others because we want
   * to make sure that age is a number.
   */
  const handleAgeChange = (event) => {
    setChar({
      age: parseInt(event.target.value)
    })
  }

  // here we can write javascript normally, inside of the parathensis is 'JSX' (javascript/HTML mixed)
  return (
    <div className="app">
      <div className="editor">
        { /* we can use HTML just like normal... but use {} to put in JS. */ }
        <h1>Create a new Character</h1>
        { /* this field specifies a name which is a simple text field */ }
        <Field label="Name">
          <input type="text" value={ char.name } onChange={ handleChange('name') } />
        </Field>
        { /* Selects from a radio button group */ }
        <SexSelector sex={ char.sex } onChange={ handleChange('sex') } />
        { /* we pass in all the valid races to the race selector.  */ }
        <Field label="Race">
          <ListSelector value={ char.race } items={Races} onChange={ handleChange('race') } />
        </Field>
        { /* We can reuse our selector for class as well */ }
        <Field label="Class">
          <ListSelector value={ char.class } items={Classes} onChange={ handleChange('class') } />
        </Field>
        { /* this field specifies a number with a range. */ }
        <Field label="Age">
          <input type="number" min="1" max="999" value = { char.age } onChange={ handleAgeChange } />
          <AgeInfo age={ char.age } race={ char.race } />
        </Field>

      </div>
      <div className="display">
        <CharDisplay { ...char /* this line passes in ALL our state as props, ... is a new ES6 operator */ } />
      </div>
    </div>
  );
}

// this actually tells react where to render the components and to initiate it.
ReactDOM.render(
  <App />,
  document.getElementById('root')
);